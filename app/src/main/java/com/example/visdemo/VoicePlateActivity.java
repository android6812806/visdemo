package com.example.visdemo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.google.ai.client.generativeai.GenerativeModel;
import com.google.ai.client.generativeai.java.GenerativeModelFutures;
import com.google.ai.client.generativeai.type.Content;
import com.google.ai.client.generativeai.type.GenerateContentResponse;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class VoicePlateActivity extends AppCompatActivity {

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private SpeechRecognizer speechRecognizer;
    private static final int SCROLL_DELAY = 10;
    private static final int SCROLL_AMOUNT = 1;
    TextView textView;
    TextView textview2;
    TextView textview3;
    TextToSpeech textToSpeech;
    View mainLayout;
    LottieAnimationView animationView;
    LottieAnimationView animationView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_plate);
        textView=findViewById(R.id.textview22);
        textview2=findViewById(R.id.textview23);
        textview3=findViewById(R.id.textView24);
     animationView = findViewById(R.id.animation_view);
        animationView2 = findViewById(R.id.animation_view2);
        animationView2.setVisibility(View.VISIBLE);
        animationView.setVisibility(View.INVISIBLE);
        mainLayout = findViewById(R.id.main_layout);

//        Button voiceButton = findViewById(R.id.button);
//        voiceButton.setOnClickListener(v -> startSpeechRecognition());
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    // Text-to-speech initialization successful
                    Log.d("TextToSpeech", "Initialization successful");
                    int result = textToSpeech.setLanguage(Locale.UK);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        // Language not supported
                        Log.e("TextToSpeech", "Language not supported");
                    }
                } else {
                    // Text-to-speech initialization failed
                    Log.e("TextToSpeech", "Initialization failed");
                }
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startSpeechRecognition();
                checkRecordAudioPermission();
            }
        }, 2000); // 2000 milliseconds = 2 seconds


//        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                textToSpeech.speak("hello world", TextToSpeech.QUEUE_ADD, null, "jk");
////                Getresponse("hello");
////                startSpeechRecognition();
////                checkRecordAudioPermission();
//            }
//        });

        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hide or remove the layout when tapped outside
                hideLayout();
                textToSpeech.stop();
            }
        });



    }

    private void checkRecordAudioPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO_PERMISSION);
        }
    }

    private void startSpeechRecognition() {
        if (speechRecognizer == null) {
            speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
            speechRecognizer.setRecognitionListener(new RecognitionListener() {
                @Override
                public void onReadyForSpeech(Bundle params) {
                    // Called when the recognition service is ready to start listening.
                    Log.v("MyTag", "recognition service is ready to start listening");
                    textview2.setText("Speak Now");



                }

                @Override
                public void onBeginningOfSpeech() {
                    // Called when the user has started to speak.
                    Log.v("MyTag", "user has started to speak");
//                    textview2.setText("Speak Now");



                }

                @Override
                public void onRmsChanged(float rmsdB) {

                }

                @Override
                public void onBufferReceived(byte[] buffer) {

                }

                @Override
                public void onEndOfSpeech() {
                    // Called when the user has finished speaking
                    Log.v("MyTag", "user has finished speaking");
                    textview2.setVisibility(View.GONE);
                    animationView2.setVisibility(View.INVISIBLE);
                    animationView.setVisibility(View.VISIBLE);
                }

@Override
public void onResults(Bundle results) {
                    Log.v("Tag","Inside the onResults Function");
    ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
    if (matches != null && !matches.isEmpty()) {
        // Process the recognized speech
        String spokenText = matches.get(0);
                                spokenText = spokenText.toLowerCase();
                        spokenText = spokenText.substring(0, 1).toUpperCase() + spokenText.substring(1);
                        for (int i = 0; i < spokenText.length(); i++) {
                            if (spokenText.charAt(i) == ' ') {
                                spokenText = spokenText.substring(0, i + 1) + spokenText.substring(i + 1, i + 2).toUpperCase() + spokenText.substring(i + 2);
                            }
                        }

        textview3.setText(spokenText.toString());
        Getresponse(spokenText);
        // Call the Getresponse method with the spoken text

        // Speak the recognized text
//        textToSpeech.speak(spokenText, TextToSpeech.QUEUE_FLUSH, null, "jk");
    }
}



                @Override
                public void onPartialResults(Bundle partialResults) {

                }

                @Override
                public void onEvent(int eventType, Bundle params) {

                }

                // Other overridden methods

                @Override
                public void onError(int errorCode) {
                    // Called when an error occurs during recognition.
                    Log.v("MyTag", "Error occurred during recognition. Error code: " + errorCode);
                }

            });
        }

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        speechRecognizer.startListening(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted, you can now start speech recognition
            } else {
                // Permission denied
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (speechRecognizer != null) {
            speechRecognizer.destroy();
        }
    }


    private void Getresponse(String spokenText) {
        Log.v("MyTag","Inide the GetResponse Method");
        String custom=getResources().getString(R.string.custom_instruction);
        // Use a model that's applicable for your use case (see "Implement basic use cases" below)
        GenerativeModel gm = new GenerativeModel("gemini-pro", BuildConfig.API_KEY);
        GenerativeModelFutures model = GenerativeModelFutures.from(gm);
        Content content = new Content.Builder().addText(spokenText+custom).build();
        Executor executor = Executors.newSingleThreadExecutor();
        ListenableFuture<GenerateContentResponse> response = model.generateContent(content);
        Futures.addCallback(response, new FutureCallback<GenerateContentResponse>() {
            @Override
            public void onSuccess(GenerateContentResponse result) {
                String resultText = result.getText();
                System.out.println(resultText);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        animationView.setVisibility(View.GONE);
                        textView.setText("\"" + resultText + "\"");
                        textToSpeech.speak(resultText, TextToSpeech.QUEUE_FLUSH, null, "jk");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startScrolling();
                            }
                        }, 1000);
                        String lowerCaseResult = resultText.toLowerCase();
                        if (lowerCaseResult.contains("navigating to")) {
                            // Extract location from spoken text
                            String[] parts = lowerCaseResult.split("navigating to");
                            if (parts.length > 1) {
                                String location = parts[1].trim();
                                // Replace spaces with '+' and build the URI
                                String uriLocation = location.replace(" ", "+");
                                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + uriLocation);
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                mapIntent.setPackage("com.google.android.apps.maps");
                                startActivity(mapIntent);
                            }
                        } else if (lowerCaseResult.contains("opening")) {
                            Log.v("My Tag", "Hi hazik:");
                            String[] parts = lowerCaseResult.split("opening ");
                            if (parts.length > 1) {
                                String appName = parts[1].trim();

                                Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
                                mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                                for (ResolveInfo info : getPackageManager().queryIntentActivities(mainIntent, 0)) {
                                    Log.i("Name", info.loadLabel(getPackageManager()).toString());
                                    if (appName.toLowerCase().contains(info.loadLabel(getPackageManager()).toString().toLowerCase()) || getPackageManager().toString().toLowerCase().contains(appName.toLowerCase())) {
                                        textToSpeech.speak("Opening " + appName, TextToSpeech.QUEUE_FLUSH, Bundle.EMPTY, "TTS");
                                        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(info.activityInfo.applicationInfo.packageName);
                                        startActivity(launchIntent);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                });
            }
            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        }, executor);


    }
    private void startScrolling() {
        textView.setSelected(true); // Enable text selection for marquee effect
        textView.setEllipsize(null); // Remove ellipsize to allow full text scrolling

        // Start scrolling
        textView.postDelayed(new Runnable() {
            @Override
            public void run() {
                textView.scrollBy(SCROLL_AMOUNT, 0); // Scroll text by SCROLL_AMOUNT pixels horizontally
                textView.postDelayed(this, SCROLL_DELAY); // Delay between each scroll step
            }
        }, SCROLL_DELAY); // Initial delay before starting scrolling
    }

    private void hideLayout() {
        // Hide or remove the layout from the view hierarchy
        if (speechRecognizer != null) {
            speechRecognizer.destroy();
        }

        // Finish the activity
        finish();
//        mainLayout.setVisibility(View.GONE);
    }


}