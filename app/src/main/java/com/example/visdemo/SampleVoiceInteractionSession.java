package com.example.visdemo;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.service.voice.VoiceInteractionService;
import android.service.voice.VoiceInteractionSession;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public class SampleVoiceInteractionSession extends VoiceInteractionSession {
    public SampleVoiceInteractionSession(Context context) {
        super(context);
        Log.i("Hello hai", "ctor");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onPrepareShow(Bundle args, int showFlags) {
        super.onPrepareShow(args, showFlags);
        Log.i("Hello hai", "onPrepareShow");
        setUiEnabled(false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onShow(@Nullable Bundle args, int showFlags) {
        closeSystemDialogs();
        Intent intent = new Intent(getContext(), VoicePlateActivity.class);
        startAssistantActivity(intent);
        Log.i("Hello hai", "Hello hai");
    }
}
