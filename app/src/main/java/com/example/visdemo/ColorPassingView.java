package com.example.visdemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ColorPassingView extends View {

    private static final int NUM_COLORS = 50;
    private static final int SPEED = 5; // Speed of color passing (pixels per frame)

    private List<Integer> colors;
    private List<Float> positions;
    private Paint paint;
    private Random random;

    private Handler handler;

    public ColorPassingView(Context context) {
        super(context);
        init();
    }

    public ColorPassingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ColorPassingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        colors = new ArrayList<>();
        positions = new ArrayList<>();
        paint = new Paint();
        random = new Random();

        handler = new Handler();
        startAnimation();
    }

    private void startAnimation() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updatePositions();
                invalidate(); // Trigger redraw
                handler.postDelayed(this, 50); // Repeat every 50 milliseconds
            }
        }, 50); // Start after 50 milliseconds
    }

    private void updatePositions() {
        for (int i = 0; i < positions.size(); i++) {
            float newPos = positions.get(i) - SPEED;
            if (newPos < -getWidth()) {
                newPos = getWidth();
            }
            positions.set(i, newPos);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        colors.clear();
        positions.clear();

        for (int i = 0; i < NUM_COLORS; i++) {
            colors.add(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            positions.add((float) random.nextInt(getWidth()));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (int i = 0; i < colors.size(); i++) {
            paint.setColor(colors.get(i));
            canvas.drawRect(positions.get(i), 0, positions.get(i) + 20, getHeight(), paint);
        }
    }
}
