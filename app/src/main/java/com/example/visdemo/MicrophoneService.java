package com.example.visdemo;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class MicrophoneService extends Service {

    private static final String TAG = "MicrophoneService";
    private SpeechRecognizer speechRecognizer;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "MicrophoneService onCreate");
        initializeSpeechRecognizer();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "MicrophoneService onStartCommand");
        startSpeechRecognition();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "MicrophoneService onDestroy");
        stopSpeechRecognition();
        super.onDestroy();
    }

    private void initializeSpeechRecognizer() {
        if (SpeechRecognizer.isRecognitionAvailable(getApplicationContext())) {
            speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getApplicationContext());
            speechRecognizer.setRecognitionListener(new RecognitionListener() {
                @Override
                public void onReadyForSpeech(Bundle params) {
                    Log.d(TAG, "onReadyForSpeech");
                }

                @Override
                public void onBeginningOfSpeech() {
                    Log.d(TAG, "onBeginningOfSpeech");
                }

                @Override
                public void onRmsChanged(float rmsdB) {
                    Log.d(TAG, "onRmsChanged");
                }

                @Override
                public void onBufferReceived(byte[] buffer) {
                    Log.d(TAG, "onBufferReceived");
                }

                @Override
                public void onEndOfSpeech() {
                    Log.d(TAG, "onEndOfSpeech");
                }

                @Override
                public void onError(int error) {
                    Log.d(TAG, "onError: " + error);
                    // Restart speech recognition on error
                    startSpeechRecognition();
                }

                @Override
                public void onResults(Bundle results) {
                    Log.d(TAG, "onResults");
                    ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    if (matches != null && !matches.isEmpty()) {
                        for (String spokenText : matches) {
                            Log.d(TAG, "Spoken text: " + spokenText);
                            // Check if the spoken text matches the target word or phrase
                            if (spokenText.toLowerCase().contains("hey google")) {
                                // Stop the service if the target word or phrase is detected
                                stopSelf();
                                Intent intent = new Intent(MicrophoneService.this, VoicePlateActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // Add this line
                                startActivity(intent);
                                return;
                            }
                        }
                    }
                    // Restart speech recognition after processing results
                    startSpeechRecognition();
                }

                @Override
                public void onPartialResults(Bundle partialResults) {
                    Log.d(TAG, "onPartialResults");
                    ArrayList<String> matches = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    if (matches != null && !matches.isEmpty()) {
                        for (String spokenText : matches) {
                            Log.d(TAG, "Spoken text: " + spokenText);
                            // Check if the spoken text matches the target word or phrase
                            if (spokenText.toLowerCase().contains("hey Google")) {
                                // Stop the service if the target word or phrase is detected
                                stopSelf();
                                Intent intent = new Intent(MicrophoneService.this, VoicePlateActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // Add this line
                                startActivity(intent);
                                return;
                            }
                        }
                    }
                }

                @Override
                public void onEvent(int eventType, Bundle params) {
                    Log.d(TAG, "onEvent");
                }
            });
        }
    }

    private void startSpeechRecognition() {
        try {
            // Create an intent to start speech recognition
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 50000);
            intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);

            // Start listening for speech
            speechRecognizer.startListening(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopSpeechRecognition() {
        if (speechRecognizer != null) {
            speechRecognizer.stopListening();
            speechRecognizer.cancel();
            speechRecognizer.destroy();
        }
    }
}
