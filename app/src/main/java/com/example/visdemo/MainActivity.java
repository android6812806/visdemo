package com.example.visdemo;

import android.app.role.RoleManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RoleManager roleManager = getSystemService(RoleManager.class);
        if (roleManager != null) {
            boolean isAssistant = roleManager.isRoleHeld(RoleManager.ROLE_ASSISTANT);
            if (isAssistant) {
                // Your app is set as the default assistant app
                Log.d("AssistantCheck", "Your app is the default assistant app");
            } else {
                Intent intent = new Intent(Settings.ACTION_VOICE_INPUT_SETTINGS);
                startActivity(intent);
                Log.d("AssistantCheck", "Your app is not the default assistant app");
            }
        } else {
            // RoleManager is not supported on this device
            Log.e("AssistantCheck", "RoleManager is not supported on this device");
        }



    }
}